<?php

namespace App\Tests\UnitTests\Repository;

use App\Entity\DelayedOrder;
use App\Repository\DelayedOrderRepository;
use DateTime;
use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class DelayedOrderRepositoryTest extends TestCase
{
    /**
     * @var DelayedOrderRepository
     */
    private $repository;

    /**
     * @var MockObject
     */
    private $registry, $em, $entityMetadata, $query, $queryBuilder;

    protected function setUp(): void
    {
        $this->entityMetadata = $this->createMock(ClassMetadata::class);
        $this->em = $this->createMock(EntityManager::class);
        $this->registry = $this->createMock(ManagerRegistry::class);
        $this->query = $this->createMock(AbstractQuery::class);
        $this->queryBuilder = $this->createMock(QueryBuilder::class);

        $this->em->method('getClassMetadata')->willReturn($this->entityMetadata);
        $this->registry->method('getManagerForClass')->willReturn($this->em);


        $this->repository = new DelayedOrderRepository(
            $this->registry
        );
    }

    public function testGetCurrentDelayedOrders(): void
    {
        $data = [
            'dateStart' => '2022-04-20',
            'dateEnd' => '2022-04-28'
        ];


        $delayedOrder = new delayedOrder();
        $delayedOrder->setId(1);
        $delayedOrder->setDeliveryTime(new DateTime('2022-04-24'));

        $this->em->method('createQueryBuilder')->willReturn($this->queryBuilder);

        $this->queryBuilder->method('select')->willReturn($this->queryBuilder);
        $this->queryBuilder->method('from')->willReturn($this->queryBuilder);
        $this->queryBuilder->method('orderBy')->willReturn($this->queryBuilder);
        $this->queryBuilder->method('getQuery')->willReturn($this->query);
        $this->query->method('getResult')->willReturn([$delayedOrder]);

        $this->assertEquals([$delayedOrder], $this->repository->getDelayedOrders($data));
    }

    public function testSave(): void
    {
        $delayedOrder = new DelayedOrder();

        $this->em->method('persist')->willReturn(null);
        $this->em->method('flush')->willReturn(null);

        $this->assertEquals(null, $this->repository->save($delayedOrder));
    }
}
