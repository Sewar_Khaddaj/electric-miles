<?php

namespace App\Tests\UnitTests\Repository;

use App\Entity\Order;
use App\Repository\OrderRepository;
use DateTime;
use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class OrderRepositoryTest extends TestCase
{
    /**
     * @var OrderRepository
     */
    private $repository;

    /**
     * @var MockObject
     */
    private $registry, $em, $entityMetadata, $query, $queryBuilder;

    protected function setUp(): void
    {
        $this->entityMetadata = $this->createMock(ClassMetadata::class);
        $this->em = $this->createMock(EntityManager::class);
        $this->registry = $this->createMock(ManagerRegistry::class);
        $this->query = $this->createMock(AbstractQuery::class);
        $this->queryBuilder = $this->createMock(QueryBuilder::class);

        $this->em->method('getClassMetadata')->willReturn($this->entityMetadata);
        $this->registry->method('getManagerForClass')->willReturn($this->em);


        $this->repository = new OrderRepository(
            $this->registry
        );
    }

    public function testGetOrders(): void
    {
        $data = [
            'status' => Order::STATUS_COMPLETED,
            'orderId' => 1
        ];

        $order = new Order();
        $order->setId(1);
        $order->setStatus(Order::STATUS_COMPLETED);

        $this->em->method('createQueryBuilder')->willReturn($this->queryBuilder);

        $this->queryBuilder->method('select')->willReturn($this->queryBuilder);
        $this->queryBuilder->method('from')->willReturn($this->queryBuilder);
        $this->queryBuilder->method('orderBy')->willReturn($this->queryBuilder);
        $this->queryBuilder->method('getQuery')->willReturn($this->query);
        $this->query->method('getResult')->willReturn([$order]);

        $this->assertEquals([$order], $this->repository->getOrders($data));
    }

    public function testGetCurrentDelayedOrders(): void
    {

        $order = new Order();
        $order->setId(1);
        $order->setStatus(Order::STATUS_COMPLETED);
        $order->setDeliveryTime(new DateTime('2020-04-26'));

        $this->em->method('createQueryBuilder')->willReturn($this->queryBuilder);

        $this->queryBuilder->method('select')->willReturn($this->queryBuilder);
        $this->queryBuilder->method('from')->willReturn($this->queryBuilder);
        $this->queryBuilder->method('orderBy')->willReturn($this->queryBuilder);
        $this->queryBuilder->method('getQuery')->willReturn($this->query);
        $this->query->method('getResult')->willReturn([$order]);

        $this->assertEquals([$order], $this->repository->getCurrentDelayedOrders());
    }

    public function testSave(): void
    {
        $order = new Order();

        $this->em->method('persist')->willReturn(null);
        $this->em->method('flush')->willReturn(null);

        $this->assertEquals(null, $this->repository->save($order));
    }
}
