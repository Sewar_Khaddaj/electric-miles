<?php

namespace App\Tests\UnitTests\Service\Util;


use App\Entity\Customer;
use App\Entity\Item;
use App\Entity\Order;
use App\Repository\CustomerRepository;
use App\Repository\DelayedOrderRepository;
use App\Repository\ItemRepository;
use App\Repository\OrderItemRepository;
use App\Repository\OrderRepository;
use App\Service\Util\OrderService;
use DateTime;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class OrderServiceTest extends TestCase
{

    /**
     * @var OrderService
     */
    private $service;

    /**
     * @var MockObject
     */
    private $orderRepository, $customerRepository, $orderItemRepository, $itemRepository, $delayedOrderRepository, $validator;


    protected function setUp(): void
    {

        $this->orderRepository = $this->createMock(OrderRepository::class);
        $this->customerRepository = $this->createMock(CustomerRepository::class);
        $this->orderItemRepository = $this->createMock(OrderItemRepository::class);
        $this->itemRepository = $this->createMock(ItemRepository::class);
        $this->delayedOrderRepository = $this->createMock(DelayedOrderRepository::class);
        $this->validator = $this->createMock(ValidatorInterface::class);

        $this->service = new OrderService(
            $this->orderRepository,
            $this->orderItemRepository,
            $this->itemRepository,
            $this->customerRepository,
            $this->delayedOrderRepository,
            $this->validator
        );
    }

    public function testCreateOrderSuccess(): void
    {
        $data = [
            'deliveryAddress' => 'deliveryAddress',
            'billingAddress' => 'billingAddress',
            'deliveryTime' => '2022-04-25 01:11:55',
            'customerId' => 1,
            'items' => [
                'itemIds' => [
                    1
                ],
                'itemQuantity' => [
                    1
                ]
            ]
        ];

        $customer = new Customer();
        $customer->setId(1);
        $customer->setEmail('email@email.com');

        $item = new Item();
        $item->setId(1);
        $item->setName('itemName');

        $this->validator->method('validate')->willReturn([]);
        $this->customerRepository->method('find')->willReturn($customer);
        $this->itemRepository->method('find')->willReturnOnConsecutiveCalls($item);
        $this->orderRepository->method('save');
        $result = $this->service->createOrder($data);
        $responseData = json_decode($result->getContent(), true);

        $this->assertEquals(200, $result->getStatusCode());
        $this->assertNotEmpty($result->getContent());
        $this->assertArrayHasKey('id', $responseData['data']);
        $this->assertArrayHasKey('deliveryTime', $responseData['data']);
        $this->assertArrayHasKey('status', $responseData['data']);
    }

    public function testCreateOrderErrorCustomerNotExist(): void
    {
        $data = [
            'deliveryAddress' => 'deliveryAddress',
            'billingAddress' => 'billingAddress',
            'deliveryTime' => '2022-04-25 01:11:55',
            'customerId' => 1,
            'items' => [
                'itemIds' => [
                    1
                ],
                'itemQuantity' => [
                    1
                ]
            ]
        ];


        $this->validator->method('validate')->willReturn([]);
        $result = $this->service->createOrder($data);

        $resultMock = new JsonResponse([
            'code' => Response::HTTP_BAD_REQUEST,
            'msg' => $this->service::ERROR_CUSTOMER_NOT_EXIST
        ]);

        self::assertEquals($resultMock->getContent(), $result->getContent());
    }

    public function testCreateOrderErrorItemNotExist(): void
    {
        $data = [
            'deliveryAddress' => 'deliveryAddress',
            'billingAddress' => 'billingAddress',
            'deliveryTime' => '2022-04-25 01:11:55',
            'customerId' => 1,
            'items' => [
                'itemIds' => [
                    1
                ],
                'itemQuantity' => [
                    1
                ]
            ]
        ];

        $customer = new Customer();
        $customer->setId(1);
        $customer->setEmail('email@email.com');

        $this->validator->method('validate')->willReturn([]);
        $this->customerRepository->method('find')->willReturn($customer);

        $result = $this->service->createOrder($data);

        $resultMock = new JsonResponse([
            'code' => Response::HTTP_BAD_REQUEST,
            'msg' => $this->service::ERROR_ITEM_NOT_EXIST
        ]);

        self::assertEquals($resultMock->getContent(), $result->getContent());
    }

    public function testUpdateOrderSuccess(): void
    {
        $data = [
            'orderId' => 1,
            'status' => Order::STATUS_DELAYED
        ];

        $order = new Order();
        $order->setId(1);
        $order->setStatus(Order::STATUS_READY);

        $this->orderRepository->method('find')->willReturn($order);
        $result = $this->service->updateOrder($data);

        $objectSerializer = $order->serializer();
        unset($objectSerializer['items']);

        $resultMock = new JsonResponse([
            'code' => Response::HTTP_OK,
            'msg' => $this->service::SUCCESS,
            'data' => $objectSerializer
        ]);

        self::assertEquals(200, $result->getStatusCode());
        self::assertEquals($resultMock->getContent(), $result->getContent());
    }

    public function testUpdateOrderErrorInvalidStatus(): void
    {
        $data = [
            'orderId' => 1,
            'status' => 5
        ];

        $order = new Order();
        $order->setId(1);
        $order->setStatus(Order::STATUS_READY);

        $this->orderRepository->method('find')->willReturn($order);
        $result = $this->service->updateOrder($data);

        $resultMock = new JsonResponse([
            'code' => Response::HTTP_BAD_REQUEST,
            'msg' => $this->service::ERROR_STATUS_NOT_VALID
        ]);

        self::assertEquals($resultMock->getContent(), $result->getContent());
    }
}
