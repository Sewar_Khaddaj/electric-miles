# Electricmiles Assignment - Symfony 

### Installing

Install the project's dependencies.

if you are not in electricmiles-test directory then:
```bash
cd electricmiles-test
```
install dependencies by composer
```bash
composer install
```
...

### Configure .env file
#### set db_user & db_password
DATABASE_URL=mysql://db_user:db_password@127.0.0.1:3306/db_name.

...

### Run Migrations & Load Fake Data
#### Create database && Run migrations
```bash
php bin/console doctrine:database:create
```
```bash
php bin/console doctrine:migrations:migrate
```
#### Create fake data
```bash
php bin/console doctrine:fixtures:load
```
...

Run server locally (http://localhost:8000)
```bash
symfony server:start --port=8000
```
...

## Testing
```bash
php bin/phpunit
```
...

## Command
```bash
php bin/console orders:check-delayed
```