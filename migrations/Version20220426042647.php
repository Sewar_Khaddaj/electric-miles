<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220426042647 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE `customers` (id INT AUTO_INCREMENT NOT NULL, full_name VARCHAR(255) DEFAULT \'NULL\', email VARCHAR(255) NOT NULL, phone VARCHAR(255) DEFAULT \'NULL\', created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, UNIQUE INDEX idx_email (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE `delayed_orders` (id INT AUTO_INCREMENT NOT NULL, order_id INT DEFAULT NULL, `current_time` DATETIME NOT NULL, delivery_time DATETIME NOT NULL, created_at DATETIME NOT NULL, INDEX IDX_226D79228D9F6D38 (order_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE `items` (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, price NUMERIC(5, 2) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, UNIQUE INDEX idx_name (name), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE `order_items` (id INT AUTO_INCREMENT NOT NULL, order_id INT DEFAULT NULL, item_id INT DEFAULT NULL, quantity INT NOT NULL, created_at DATETIME NOT NULL, INDEX IDX_62809DB08D9F6D38 (order_id), INDEX IDX_62809DB0126F525E (item_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE `orders` (id INT AUTO_INCREMENT NOT NULL, customer_id INT DEFAULT NULL, delivery_address VARCHAR(255) NOT NULL, billing_address VARCHAR(255) NOT NULL, status INT DEFAULT 1 NOT NULL, delivery_time DATETIME NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_E52FFDEE9395C3F3 (customer_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE `delayed_orders` ADD CONSTRAINT FK_226D79228D9F6D38 FOREIGN KEY (order_id) REFERENCES `orders` (id)');
        $this->addSql('ALTER TABLE `order_items` ADD CONSTRAINT FK_62809DB08D9F6D38 FOREIGN KEY (order_id) REFERENCES `orders` (id)');
        $this->addSql('ALTER TABLE `order_items` ADD CONSTRAINT FK_62809DB0126F525E FOREIGN KEY (item_id) REFERENCES `items` (id)');
        $this->addSql('ALTER TABLE `orders` ADD CONSTRAINT FK_E52FFDEE9395C3F3 FOREIGN KEY (customer_id) REFERENCES `customers` (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE `orders` DROP FOREIGN KEY FK_E52FFDEE9395C3F3');
        $this->addSql('ALTER TABLE `order_items` DROP FOREIGN KEY FK_62809DB0126F525E');
        $this->addSql('ALTER TABLE `delayed_orders` DROP FOREIGN KEY FK_226D79228D9F6D38');
        $this->addSql('ALTER TABLE `order_items` DROP FOREIGN KEY FK_62809DB08D9F6D38');
        $this->addSql('DROP TABLE `customers`');
        $this->addSql('DROP TABLE `delayed_orders`');
        $this->addSql('DROP TABLE `items`');
        $this->addSql('DROP TABLE `order_items`');
        $this->addSql('DROP TABLE `orders`');
    }
}
