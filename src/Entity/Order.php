<?php

namespace App\Entity;

use App\Repository\OrderRepository;
use App\Service\Util\SerializerService;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Order
 * @ORM\Table(name="`orders`")
 * @ORM\Entity(repositoryClass="App\Repository\OrderRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Order
{
    const STATUS_READY       = 1;
    const STATUS_IN_PROGRESS = 2;
    const STATUS_COMPLETED   = 3;
    const STATUS_DELAYED     = 4;

    const STATUS_NAME = [
        1 => 'Ready',
        2 => 'In progress',
        3 => 'Completed',
        4 => 'Delayed'
    ];

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="delivery_address", type="string", length=255, nullable=false)
     */
    private $deliveryAddress;

    /**
     * @var string
     *
     * @ORM\Column(name="billing_address", type="string", length=255, nullable=false)
     */
    private $billingAddress;

    /**
     * @var int
     *
     * @ORM\Column(name="status", type="integer", nullable=false, options={"default"=1})
     */
    private $status = self::STATUS_READY;

    /**
     * @var Customer
     *
     * @ORM\ManyToOne(targetEntity="Customer")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="customer_id", referencedColumnName="id")
     * })
     *
     */
    private $customer;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="delivery_time", type="datetime", nullable=false)
     */
    private $deliveryTime;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=false)
     */
    private $updatedAt;

    /**
     * @var Collection
     * @ORM\OneToMany(targetEntity="OrderItem", mappedBy="order")
     */
    private $items;

    /**
     * Order constructor.
     */
    public function __construct()
    {
        $this->items = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Order
     */
    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return Customer|null
     */
    public function getCustomer(): ?Customer
    {
        return $this->customer;
    }

    /**
     * @param  Customer|null $customer
     * @return Order
     */
    public function setCustomer(?Customer $customer): self
    {
        $this->customer = $customer;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getdeliveryAddress(): ?string
    {
        return $this->deliveryAddress;
    }

    /**
     * @param  string|null $deliveryAddress
     * @return Order
     */
    public function setDeliveryAddress(?string $deliveryAddress): self
    {
        $this->deliveryAddress = $deliveryAddress;

        return $this;
    }

    /**
     * @return string
     */
    public function getBillingAddress()
    {
        return $this->billingAddress;
    }

    /**
     *
     * @param string $billingAddress
     * @return Order
     */
    public function setBillingAddress($billingAddress)
    {
        $this->billingAddress = $billingAddress;

        return $this;
    }

    public function getStatus(): ?int
    {
        return $this->status;
    }

    public function setStatus(?int $status): self
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return DateTime|null
     */
    public function getDeliveryTime(): ?DateTime
    {
        return $this->deliveryTime;
    }

    /**
     * @param  DateTime $deliveryTime
     * @return Order
     */
    public function setDeliveryTime(DateTime $deliveryTime): self
    {
        $this->deliveryTime = $deliveryTime;

        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getItems(): ArrayCollection
    {
        $arrayCollection = new ArrayCollection();

        /**
         * @var Item $item
         */
        foreach ($this->items as $item) {
            $arrayCollection->add($item);
        }

        return $arrayCollection;
    }

    /**
     * @param Item $item
     * @return $this
     */
    public function setItems(Item $item): self
    {
        $this->items->add($item);

        return $this;
    }

    /**
     * @return DateTime|null
     */
    public function getCreatedAt(): ?DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param  DateTime $createdAt
     * @return Order
     */
    public function setCreatedAt(DateTime $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @return DateTime|null
     */
    public function getUpdatedAt(): ?DateTime
    {
        return $this->updatedAt;
    }

    /**
     * @param  DateTime $updatedAt
     * @return Order
     */
    public function setUpdatedAt(DateTime $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * @return array
     */
    public function serializer(): array
    {
        $service = new SerializerService();

        $serializer = $service->objectToJson();

        $result = [];

        $entityJson = $serializer->serialize(
            $this,
            'json',
            [
                /**
                 * @codeCoverageIgnore
                 */
                'circular_reference_handler' => function ($object) {
                    return $object->getId();
                }
            ]
        );

        if ($entityJson !== null) {
            $result = json_decode($entityJson, true);
        }

        return $result;
    }

    /**
     * @ORM\PrePersist()
     */
    public function beforeAdd()
    {
        $dateTime = new \DateTime();
        $this->setCreatedAt($dateTime);
        $this->setUpdatedAt($dateTime);
    }

    /**
     * @ORM\PreUpdate()
     */
    public function beforeUpdate()
    {
        $this->setUpdatedAt(new \DateTime());
    }

    /**
     * Virtual getter for status name
     */
    public function getStatusName()
    {
        return self::STATUS_NAME[$this->getStatus()];
    }
}
