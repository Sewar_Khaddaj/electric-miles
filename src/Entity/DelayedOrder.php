<?php

namespace App\Entity;

use App\Repository\DelayedOrderRepository;
use App\Service\Util\SerializerService;
use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * DelayedOrder
 * @ORM\Table(name="`delayed_orders`")
 * @ORM\Entity(repositoryClass="App\Repository\DelayedOrderRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class DelayedOrder
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var Order
     *
     * @ORM\ManyToOne(targetEntity="Order")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="order_id", referencedColumnName="id")
     * })
     *
     */
    private $order;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="`current_time`", type="datetime", nullable=false)
     */
    private $currentTime;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="delivery_time", type="datetime", nullable=false)
     */
    private $deliveryTime;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return DelayedOrder
     */
    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return Order|null
     */
    public function getOrder(): ?Order
    {
        return $this->order;
    }

    /**
     * @param  Order|null $order
     * @return DelayedOrder
     */
    public function setOrder(?Order $order): self
    {
        $this->order = $order;

        return $this;
    }

    /**
     * @return DateTime|null
     */
    public function getCurrentTime(): ?DateTime
    {
        return $this->currentTime;
    }

    /**
     * @param  DateTime $currentTime
     * @return DelayedOrder
     */
    public function setCurrentTime(DateTime $currentTime): self
    {
        $this->currentTime = $currentTime;

        return $this;
    }

    /**
     * @return DateTime|null
     */
    public function getDeliveryTime(): ?DateTime
    {
        return $this->deliveryTime;
    }

    /**
     * @param  DateTime $deliveryTime
     * @return DelayedOrder
     */
    public function setDeliveryTime(DateTime $deliveryTime): self
    {
        $this->deliveryTime = $deliveryTime;

        return $this;
    }

    /**
     * @return DateTime|null
     */
    public function getCreatedAt(): ?DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param  DateTime $createdAt
     * @return DelayedOrder
     */
    public function setCreatedAt(DateTime $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @return array
     */
    public function serializer(): array
    {
        $service = new SerializerService();
        $serializer = $service->objectToJson();

        $result = [];
        $entityJson = $serializer->serialize(
            $this,
            'json',
            [
                /**
                 * @codeCoverageIgnore
                 */
                'circular_reference_handler' => function ($object) {
                    return $object->getId();
                }
            ]
        );

        if ($entityJson !== null) {
            $result = json_decode($entityJson, true);
        }

        return $result;
    }

    /**
     * @ORM\PrePersist()
     */
    public function beforeAdd()
    {
        $dateTime = new \DateTime();
        $this->setCreatedAt($dateTime);
    }
}
