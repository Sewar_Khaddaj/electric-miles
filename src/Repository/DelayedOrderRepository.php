<?php

namespace App\Repository;

use App\Entity\DelayedOrder;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Query\Parameter;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method DelayedOrder|null find($id, $lockMode = null, $lockVersion = null)
 * @method DelayedOrder|null findOneBy(array $criteria, array $orderBy = null)
 * @method DelayedOrder[]    findAll()
 * @method DelayedOrder[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DelayedOrderRepository extends ServiceEntityRepository
{

    /**
     * DelayedOrderRepository constructor.
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DelayedOrder::class);
    }

    /**
     * @param DelayedOrder $delayedOrder
     * @throws ORMException
     */
    public function save(DelayedOrder $delayedOrder): void
    {
        $this->_em->persist($delayedOrder);
        $this->_em->flush();
    }

    /**
     * @param array $data
     * @return array
     */
    public function getDelayedOrders(array $data): array
    {
        $queryBuilder = $this->createQueryBuilder('do');

        $queryBuilder->andWhere('do.deliveryTime BETWEEN :dateStart AND :dateEnd');
        $queryBuilder->setParameters(new ArrayCollection([
            new Parameter('dateStart', $data['dateStart']),
            new Parameter('dateEnd', $data['dateEnd'])
        ]));

        $queryBuilder->orderBy('do.createdAt', 'DESC');
        $query = $queryBuilder->getQuery();
        return $query->getResult();
    }
}
