<?php

namespace App\Repository;

use App\Entity\Order;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\ORMException;

/**
 * @method Order|null find($id, $lockMode = null, $lockVersion = null)
 * @method Order|null findOneBy(array $criteria, array $orderBy = null)
 * @method Order[]    findAll()
 * @method Order[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OrderRepository extends ServiceEntityRepository
{

    /**
     * OrderRepository constructor.
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Order::class);
    }

    /**
     * @param Order $order
     * @throws ORMException
     */
    public function save(Order $order): void
    {
        $this->_em->persist($order);
        $this->_em->flush();
    }

    /**
     * @param array $data
     * @return array
     */
    public function getOrders(array $data): array
    {
        $queryBuilder = $this->createQueryBuilder('o');

        if (array_key_exists('status', $data)) {
            $queryBuilder->setParameter('status', $data['status']);
            $queryBuilder->andWhere('o.status = :status');
        }

        if (array_key_exists('orderId', $data)) {
            $queryBuilder->setParameter('orderId', $data['orderId']);
            $queryBuilder->andWhere('o.id = :orderId');
        }

        $queryBuilder->orderBy('o.createdAt', 'DESC');
        $query = $queryBuilder->getQuery();
        return $query->getResult();
    }

    /**
     * @return array
     */
    public function getCurrentDelayedOrders(): array
    {
        $queryBuilder = $this->createQueryBuilder('o');
        $queryBuilder->andWhere('o.deliveryTime < CURRENT_DATE()');
        $query = $queryBuilder->getQuery();
        return $query->getResult();
    }
}
