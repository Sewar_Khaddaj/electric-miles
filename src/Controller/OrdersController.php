<?php

namespace App\Controller;

use App\Service\Util\OrderService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class OrdersController extends AbstractController
{
    /**
     * @var OrderService
     */
    private $orderService;

    public function __construct(OrderService $orderService)
    {
        $this->orderService = $orderService;
    }

    /**
     * @param  Request      $request
     * @return JsonResponse
     */
    public function createOrder(Request $request): JsonResponse
    {
        $data = $request->request->all();
        return $this->orderService->createOrder($data);
    }

    /**
     * @param  Request      $request
     * @param  string       $orderId
     * @return JsonResponse
     */
    public function updateOrder(Request $request, string $orderId): JsonResponse
    {
        $data = $request->request->all();
        $data['orderId'] = $orderId;
        return $this->orderService->updateOrder($data);
    }

    /**
     * @param  Request      $request
     * @return JsonResponse
     */
    public function getOrders(Request $request): JsonResponse
    {
        $data = $request->query->all();
        return $this->orderService->getOrders($data);
    }

    /**
     * @param  Request      $request
     * @return JsonResponse
     */
    public function getDelayedOrders(Request $request): JsonResponse
    {
        $data = $request->query->all();
        return $this->orderService->getDelayedOrders($data);
    }
}
