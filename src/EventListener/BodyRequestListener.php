<?php

namespace App\EventListener;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\Exception\HttpException;

class BodyRequestListener
{

    /**
     * @param RequestEvent $event
     */
    public function onKernelRequest(RequestEvent $event): void
    {
        $request = $event->getRequest();
        if ($this->containsHeader($request, 'Content-Type', 'application/json')) {
            $jsonData = json_decode($request->getContent(), true);
            if (!$jsonData) {
                throw new HttpException(Response::HTTP_BAD_REQUEST, 'Invalid json data');
            }
            $request->request->replace($jsonData);
        }
    }

    /**
     * @param Request $request
     * @param $name
     * @param $value
     * @return bool
     */
    private function containsHeader(Request $request, $name, $value): bool
    {
        return 0 === strpos($request->headers->get($name), $value);
    }
}
