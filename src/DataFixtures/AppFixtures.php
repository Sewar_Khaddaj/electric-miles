<?php

namespace App\DataFixtures;

use App\Entity\Customer;
use App\Entity\Item;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    private const CUSTOMERS = [
        [
            'fullName' => 'tom_hanks',
            'email' => 'tom_hanks@hanks.com',
            'phone' => '+333 333 333',
        ],
        [
            'fullName' => 'will_smith',
            'email' => 'will_smith@smith.com',
            'phone' => '+444 444 444',
        ],
        [
            'fullName' => 'emma_stone',
            'email' => 'emma_stone@stone.com',
            'phone' => '+555 555 555',
        ],
    ];

    private const ITEMS = [
        [
            'name' => 'item_1',
            'price' => 100
        ],
        [
            'name' => 'item_2',
            'price' => 200
        ],
        [
            'name' => 'item_3',
            'price' => 300
        ],
    ];

    public function load(ObjectManager $manager)
    {
        $this->loadCustomers($manager);
        $this->loadItems($manager);
    }

    public function loadCustomers(ObjectManager $manager)
    {
        foreach(self::CUSTOMERS as $customerData)
        {
            $customer = new Customer();
            $customer->setFullName($customerData['fullName']);
            $customer->setEmail($customerData['email']);
            $customer->setPhone( $customerData['phone']);
            $manager->persist($customer);
        }

        $manager->flush();
    }

    public function loadItems(ObjectManager $manager)
    {
        foreach(self::ITEMS as $itemData)
        {
            $item = new Item();
            $item->setName($itemData['name']);
            $item->setPrice($itemData['price']);
            $manager->persist($item);
        }

        $manager->flush();
    }
}