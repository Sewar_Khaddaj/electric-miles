<?php

namespace App\Service\Util;

use App\Entity\Customer;
use App\Entity\Item;
use App\Entity\Order;
use App\Entity\OrderItem;
use App\Repository\CustomerRepository;
use App\Repository\DelayedOrderRepository;
use App\Repository\ItemRepository;
use App\Repository\OrderItemRepository;
use App\Repository\OrderRepository;
use DateTime;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class OrderService
{
    public const SUCCESS = 'SUCCESS';
    public const ERROR_CUSTOMER_NOT_EXIST = 'Error customer not exist';
    public const ERROR_ITEM_NOT_EXIST     = 'Error item not exist';
    public const ERROR_ORDER_NOT_EXIST    = 'Error order not exist';
    public const ERROR_STATUS_NOT_VALID   = 'Error status not valid';
    public const ERROR_STATUS_REQUIRED    = 'Error status is required field';
    public const ERROR_DATES_REQUIRED     = 'Error dateStart and dateEnd are required fields';

    public const VALID_STATUS = [
        Order::STATUS_COMPLETED,
        Order::STATUS_DELAYED,
        Order::STATUS_IN_PROGRESS,
        Order::STATUS_READY
    ];

    /**
     * @var OrderRepository
     */
    private $orderRepository;

    /**
     * @var CustomerRepository
     */
    private $customerRepository;

    /**
     * @var OrderItemRepository
     */
    private $orderItemRepository;

    /**
     * @var ItemRepository
     */
    private $itemRepository;

    /**
     * @var DelayedOrderRepository
     */
    private $delayedOrderRepository;

    /**
     * @var ValidatorInterface
     */
    private $validator;

    /**
     * OrderService constructor.
     * @param OrderRepository $orderRepository
     * @param OrderItemRepository $orderItemRepository
     * @param ItemRepository $itemRepository
     * @param CustomerRepository $customerRepository
     * @param DelayedOrderRepository $delayedOrderRepository
     * @param ValidatorInterface $validator
     */
    public function __construct(
        OrderRepository $orderRepository,
        OrderItemRepository $orderItemRepository,
        ItemRepository $itemRepository,
        CustomerRepository $customerRepository,
        DelayedOrderRepository $delayedOrderRepository,
        ValidatorInterface $validator
    ) {
        $this->orderRepository        = $orderRepository;
        $this->orderItemRepository    = $orderItemRepository;
        $this->itemRepository         = $itemRepository;
        $this->customerRepository     = $customerRepository;
        $this->delayedOrderRepository = $delayedOrderRepository;
        $this->validator              = $validator;
    }

    /**
     * @param array $data
     * @return JsonResponse
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function createOrder(array $data): JsonResponse
    {
        $paramsValidated = $this->validateParams($data);

        if ($paramsValidated['msg'] !== $this::SUCCESS) {
            return new JsonResponse([
                'code' => Response::HTTP_BAD_REQUEST,
                'msg' => $paramsValidated['msg']
            ]);
        }

        $order = $this->createEntities($data, $paramsValidated);

        return new JsonResponse([
            'code' => Response::HTTP_OK,
            'msg' => $this::SUCCESS,
            'data' => [
                'id'  => $order->getId(),
                'status' => $order->getStatusName(),
                'deliveryTime' => $order->getDeliveryTime()->format('Y-m-d H:i:s')
            ]
        ]);
    }

    /**
     * @param array $data
     * @return JsonResponse
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function updateOrder(array $data): JsonResponse
    {
        $paramsValidated = $this->validateUpdateOrderParams($data);

        if ($paramsValidated['msg'] !== $this::SUCCESS) {
            return new JsonResponse([
                'code' => Response::HTTP_BAD_REQUEST,
                'msg' => $paramsValidated['msg']
            ]);
        }

        $order = $this->updateOrderStatus($paramsValidated['status'], $paramsValidated['order']);
        $response = $order->serializer();
        $response = $this->formatOrderItemData($response);

        return new JsonResponse([
            'code' => Response::HTTP_OK,
            'msg'  => $this::SUCCESS,
            'data' => $response
        ]);
    }

    /**
     * @param array $data
     * @return JsonResponse
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function getOrders(array $data): JsonResponse
    {
        $orders = $this->orderRepository->getOrders($data);
        $data = [];

        foreach ($orders as $index => $order) {
            $data[] = $order->serializer();
            $data[$index] = $this->formatOrderItemData($data[$index]);
        }

        return new JsonResponse([
            'code' => Response::HTTP_OK,
            'msg' => $this::SUCCESS,
            'data' => $data
        ]);
    }

    /**
     * @param array $data
     * @return JsonResponse
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function getDelayedOrders(array $data): JsonResponse
    {
        if (!array_key_exists('dateEnd', $data) || !array_key_exists('dateStart', $data)) {
            return new JsonResponse([
                'code' => Response::HTTP_BAD_REQUEST,
                'msg' => $this::ERROR_DATES_REQUIRED
            ]);
        }

        $orders = $this->delayedOrderRepository->getDelayedOrders($data);
        $data = [];

        foreach ($orders as $index => $order) {
            $data[] = $order->serializer();
            $data[$index]['orderId'] = $data[$index]['order']['id'];
            unset($data[$index]['order']);
        }

        return new JsonResponse([
            'code' => Response::HTTP_OK,
            'msg' => $this::SUCCESS,
            'data' => $data
        ]);
    }

    /**
     * @param array $data
     * @param array $paramsValidated
     * @return Order
     * @throws ORMException
     * @throws OptimisticLockException
     */
    private function createEntities(array $data, array $paramsValidated): Order
    {
        $order = $this->createEntityOrder($data, $paramsValidated['customer']);
        $this->createEntityOrderItem($data['items']['itemQuantity'], $paramsValidated['items'], $order);
        return $order;
    }

    /**
     * @param array $quantities
     * @param array $items
     * @param Order $order
     * @return void
     * @throws ORMException
     * @throws OptimisticLockException
     */
    private function createEntityOrderItem(array $quantities, array $items, Order $order): void
    {
        foreach ($items as $key => $item) {
            $orderItem = new OrderItem();
            $orderItem->setItem($item);
            $orderItem->setOrder($order);
            $orderItem->setQuantity($quantities[$key]);
            $this->orderItemRepository->onlyPersist($orderItem);
        }

        $this->orderItemRepository->onlyFlush();
    }

    /**
     * @param array $data
     * @param Customer $customer
     * @return Order
     * @throws ORMException
     * @throws OptimisticLockException
     */
    private function createEntityOrder(array $data, Customer $customer): Order
    {
        $order = new Order();
        $order->setDeliveryAddress($data['deliveryAddress']);
        $order->setBillingAddress($data['billingAddress']);
        $order->setDeliveryTime(new DateTime($data['deliveryTime']));
        $order->setCustomer($customer);
        $this->orderRepository->save($order);
        return $order;
    }

    /**
     * @param int $status
     * @param Order $order
     * @return Order
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function updateOrderStatus(int $status, Order $order): Order
    {
        $order->setStatus($status);
        $this->orderRepository->save($order);
        return $order;
    }

    /**
     * @param array $data
     * @return array
     */
    private function validateParams(array $data): array
    {
        $violations = $this->globalValidation($data);
        if ($violations) {
            return ['msg' => $violations];
        }

        $customer = $this->getCustomer($data['customerId']);
        if ($customer === null) {
            return ['msg' => $this::ERROR_CUSTOMER_NOT_EXIST];
        }

        $items = [];
        foreach ($data['items']['itemIds'] as $itemId) {
            $item = $this->getItem($itemId);
            if ($item === null) {
                return ['msg' => $this::ERROR_ITEM_NOT_EXIST];
            }
            $items[] = $item;
        }

        return [
            'msg' => $this::SUCCESS,
            'customer' => $customer,
            'items' => $items
        ];
    }

    /**
     * @param array $data
     * @return array
     */
    private function validateUpdateOrderParams(array $data): array
    {
        $order = $this->getOrder($data['orderId']);
        if ($order === null) {
            return ['msg' => $this::ERROR_ORDER_NOT_EXIST];
        }

        if (!array_key_exists('status', $data)) {
            return ['msg' => $this::ERROR_STATUS_REQUIRED];
        }

        if (!in_array($data['status'], $this::VALID_STATUS)) {
            return ['msg' => $this::ERROR_STATUS_NOT_VALID];
        }

        return [
            'msg' => $this::SUCCESS,
            'order' => $order,
            'status' => $data['status']
        ];
    }

    public function globalValidation($data)
    {
        $constraints = new Assert\Collection([
            'deliveryAddress' => [new Assert\NotBlank],
            'billingAddress'  => [new Assert\NotBlank],
            'deliveryTime'    => [new Assert\DateTime],
            'customerId'      => [new Assert\NotBlank],
            'items' => new Assert\Optional([
                new Assert\Type('array'),
                new Assert\Count(['min' => 2]),
                new Assert\Collection([
                    'itemIds' => [
                        new Assert\NotBlank(),
                        new Assert\Type(['type' => 'array']),
                        new Assert\Count(['min' => 1])

                    ],
                    'itemQuantity' => [
                        new Assert\NotBlank(),
                        new Assert\Type(['type' => 'array']),
                        new Assert\Count(['min' => 1])
                    ],
                ]),
            ]),
        ]);

        $violations = $this->validator->validate($data, $constraints);
        $errorMessages = [];
        if (!empty($violations)) {
            $accessor = PropertyAccess::createPropertyAccessor();
            foreach ($violations as $violation) {
                $accessor->setValue($errorMessages, $violation->getPropertyPath(), $violation->getMessage());
            }
        }
        return $errorMessages;
    }

    /**
     * @param string $customerId
     * @return Customer|null
     */
    private function getCustomer(string $customerId): ?Customer
    {
        return $this->customerRepository->find($customerId);
    }

    /**
     * @param string $itemId
     * @return Item|null
     */
    private function getItem(string $itemId): ?Item
    {
        return $this->itemRepository->find($itemId);
    }

    /**
     * @param string $orderId
     * @return Order|null
     */
    private function getOrder(string $orderId): ?Order
    {
        return $this->orderRepository->find($orderId);
    }

    /**
     * @param  array $data
     * @return array
     */
    public function formatOrderItemData(array $data): array
    {
        $items = $data['items'];
        unset($data['items']);
        foreach ($items as $key => $value) {
            $data['items'][$key] = $value['item'];
        }
        return $data;
    }
}
