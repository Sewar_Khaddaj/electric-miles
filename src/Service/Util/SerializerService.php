<?php

namespace App\Service\Util;

use App\Service\Interfaces\SerializerInterface;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class SerializerService implements SerializerInterface
{
    /**
     * @return Serializer
     */
    public function objectToJson(): Serializer
    {
        $encoderJson = new JsonEncoder();

        $dateCallback = function ($innerObject) {
            return $innerObject instanceof \DateTime ? $innerObject->format('Y-m-d H:i:s') : '';
        };

        $normalizer = new ObjectNormalizer(
            null,
            null,
            null,
            null,
            null,
            null,
            [
                AbstractNormalizer::IGNORED_ATTRIBUTES => [
                    '__initializer__',
                    '__cloner__',
                    '__isInitialized__'
                ],
                AbstractNormalizer::CALLBACKS => [
                    'createdAt'    => $dateCallback,
                    'updatedAt'    => $dateCallback,
                    'deliveryTime' => $dateCallback,
                    'currentTime'  => $dateCallback
                ]
            ]
        );

        return new Serializer([$normalizer], [$encoderJson]);
    }
}
