<?php

namespace App\Service\Interfaces;

use Symfony\Component\Serializer\Serializer;

interface SerializerInterface
{
    /**
     * @return Serializer
     */
    public function objectToJson(): Serializer;
}