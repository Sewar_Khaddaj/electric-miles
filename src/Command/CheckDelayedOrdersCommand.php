<?php

namespace App\Command;

use App\Entity\DelayedOrder;
use App\Entity\Order;
use App\Repository\DelayedOrderRepository;
use App\Repository\OrderRepository;
use App\Service\Util\OrderService;
use DateTime;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class CheckDelayedOrdersCommand extends Command
{
    protected static $defaultName = 'orders:check-delayed';
    protected static $defaultDescription = 'Verify if the current time is greater than the orders delivery time to create new records in the delayed orders table.';

    /**
     * @var OrderRepository
     */
    private $orderRepository;

    /**
     * @var OrderService
     */
    private $orderService;

    /**
     * @var DelayedOrderRepository
     */
    private $delayedOrderRepository;

    public function __construct(OrderRepository $orderRepository, OrderService $orderService, DelayedOrderRepository $delayedOrderRepository)
    {
        parent::__construct();
        $this->orderRepository        = $orderRepository;
        $this->orderService           = $orderService;
        $this->delayedOrderRepository = $delayedOrderRepository;
    }

    protected function configure(): void
    {
        $this->setDescription(self::$defaultDescription);
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $orders = $this->orderRepository->getCurrentDelayedOrders();
        if (empty($orders)) {
            $io->error("Failed to execute the command, Couldn't find any delyed orders");
            return Command::FAILURE;
        }

        foreach ($orders as $order) {
            $this->orderService->updateOrderStatus(Order::STATUS_DELAYED, $order);
            $this->createDeyledOrder($order);
        }

        $io->success("Done");
        return Command::SUCCESS;
    }

    public function createDeyledOrder(Order $order): void
    {
        $delayedOrder = $this->getDelayedOrder($order->getId());
        if ($delayedOrder === null) {
            $delayedOrder = new DelayedOrder();
            $delayedOrder->setOrder($order);
            $delayedOrder->setDeliveryTime($order->getDeliveryTime());
            $delayedOrder->setCurrentTime(new DateTime());
            $this->delayedOrderRepository->save($delayedOrder);
        }
    }

    /**
     * @param string $orderId
     * @return DelayedOrder|null
     */
    public function getDelayedOrder(string $orderId): ?DelayedOrder
    {
        return $this->delayedOrderRepository->find($orderId);
    }
}
